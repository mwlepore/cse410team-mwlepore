#include <stdlib.h>
#include <stdio.h>
#include <stdbool.h>
#include <string.h>
#include "post.h"
#include "vector.h"
int main(int argc, char **argv){
	if(argc==2){
		VEC_INIT(records);
		
		vector* newRecords = &records;
		int hasDataSaved = 0;
		int isUserConfirming = 0;

		char* filename = argv[1];
		if(readInputFile(filename, &records)>=0){				
			while(true){

				if(isUserConfirming == 1){
					char ans[10] = {0};
                                        scanf("%s", ans);
                                        if(strcmp(ans,"y")==0){
						break;
					}
					else{
						isUserConfirming = 0;
					}
				}

				char input[1024] = {0};
				char validationInput[1024] = {0};
				printf("\n? ");
				fgets(input,1024,stdin);
				strcpy(validationInput,input);
				
				if(ifInputValid(validationInput)){
				
					char* token = strtok(input, " "); //Added tokenizer here!!!

					if(strcmp(token,"filter") == 0){	//Filter Command
						char *keyList[128];
						char *typeList[128];
						char *fieldList[128];
						int listIndex = 0;
						while(token != NULL){
							keyList[listIndex] = strtok(NULL, " ");
							typeList[listIndex] = strtok(NULL, " ");
							fieldList[listIndex] = strtok(NULL, " ");
							listIndex = listIndex + 1;
							strtok(NULL, " ");		//OR
							token = strtok(NULL, " ");	//filter
						}

						newRecords = filterRecords(newRecords, keyList, typeList, fieldList, listIndex);
					}
					else if(strcmp(token,"reset\n")==0){	//Reset Command
						newRecords = &records;

					}
					else if(strcmp(token,"save")==0){	//Save Command
						char* token = strtok(NULL," ");
						char* name = strdup(token);
						char* filename = strtok(name,"\n");
						saveRecords(filename, newRecords);
						free(name);
						hasDataSaved = 1;
					}
					else if(strcmp(token,"overwrite")==0){	//Overwrite Command
						char* token = strtok(NULL," ");
						char* name = strdup(token);
						char* filename = strtok(name,"\n");
						overwriteRecords(filename, newRecords);
						free(name);
						hasDataSaved = 1;
					}
					else if(strcmp(token,"count\n")==0){	//Count Command
						printf("The current dataset has %d records\n", vector_size(newRecords));
					}
					else if(strcmp(token,"print\n")==0){	//Print Command
						printRecords(newRecords);

					}
					else if(strcmp(token,"help\n")==0){	//Help Command
						printHelpCommands();

					}
					else if(strcmp(token,"quit\n")==0){	//Quit Command
						if(hasDataSaved == 1){
							break;
						}
						else{
							printf("Quit program? (y or n) ");
                                			isUserConfirming = 1;
						}
					}
					else{
						printf("Please enter a valid command.\n");
					}
				}
				else{
					printf("Please enter a valid command.\n");
				}
			}
			freeRecords(&records);
			VEC_FREE(records);
		}
		else{return 1;}
	}
	else{
		printf("Please provide a single argument; the input filename.\n");
		return 1;
	}
	return 0;
}
