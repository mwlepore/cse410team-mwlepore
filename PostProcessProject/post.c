#ifndef POST
#define POST
#include "post.h"
#endif
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>
#include "vector.h"



int readInputFile(char* inputfilename, vector* records){
	FILE* in;
	in = fopen(inputfilename, "r");
	if(in){
		char* line;
		struct field* f = NULL;
		struct record* r = NULL;
		for(line = calloc(1024,sizeof(char));fgets(line,1024,in)!=NULL;memset(line,0,1024)){
			if(strcmp(line,"<PRODUCT>\r\n")==0){
				r = new_record();
				VEC_ADD(*records, r);
				continue;
			}
			/* Get the next line, because the link is on the next line.  */
			if(strcmp(line,"\t<PHOTOS_LINK>\r\n")==0){
				memset(line,0,1024);
				fgets(line,1024,in);
				//printf("%s\n",line);
				char* token;
				char delim[4] = "\r\n\t";
				char* copy = strdup(line);
				token = strtok(copy, delim);
				char* result = strdup(token);
				f = new_field(PHOTOS_LINK,result);
				free(copy);
				free(result);
				addFieldtoRecord(f,r);
				if(f){
					//free(f->text);
					free(f);
				}
				continue;
			}
			f = stripTags(line);//stripInnerTags(line));
			if(f&&r){
				addFieldtoRecord(f,r);
				if(f){
					//free(f->text);
					free(f);
				}
			}
		}

		free(line);
		fclose(in);
		return VEC_SIZE(*records);
	}
	else{
		printf("\"%s\" did not open successfully.  Does it exist?\n",inputfilename);
		return -1;
	}
}

struct record* new_record(){
	struct record* r = calloc(1,sizeof(struct record));
	return r;
}

struct field* new_field(int tag, char* text){
	struct field* f = calloc(1,sizeof(struct field));
	f->tag = tag;
	f->text = strdup(text);
	return f;
}

void destroyRecord(struct record* r){
	if(r->date)free(r->date);
	if(r->brand_name)free(r->brand_name);
	if(r->reason)free(r->reason);
	if(r->photos_link)free(r->photos_link);
	if(r->product_description)free(r->product_description);
	if(r->company)free(r->company);
	if(r->company_release)free(r->company_release);
}

void freeRecords(vector* v){
	for(int i=0;i<VEC_SIZE(*v);i++){
		struct record* r = VEC_GET(*v,i,struct record*);
		destroyRecord(r);
		free(r);
	}
}

void saveRecords(char* filename, vector* v){
	FILE* out;
	out = fopen(filename,"r");

	/* Doesn't exist for reading, safe to open it for writing. */
	if(!out){
		out = fopen(filename,"w");
		fprintf(out,"<?xml version=\"1.0\" encoding=\"UTF-8\" ?>\r\n");
		fprintf(out,"<RECALLS_DATA>\r\n");
		for(int i=0;i<VEC_SIZE(*v);i++){
			fprintf(out,"<PRODUCT>\r\n");
			struct record* r = VEC_GET(*v,i,struct record*);
			fprintf(out,"\t\t<DATE>%s</DATE>\r\n",r->date);		
			fprintf(out,"\t\t<BRAND_NAME><![CDATA[%s]]></BRAND_NAME>\r\n",r->brand_name);
			fprintf(out,"\t\t<PRODUCT_DESCRIPTION><![CDATA[%s]]></PRODUCT_DESCRIPTION>\r\n",r->product_description);
			fprintf(out,"\t\t<REASON><![CDATA[%s]]></REASON>\r\n",r->reason);
			fprintf(out,"\t\t<COMPANY><![CDATA[%s]]></COMPANY>\r\n",r->company);
			fprintf(out,"\t\t<COMPANY_RELEASE_LINK>%s</COMPANY_RELEASE_LINK>\r\n",r->company_release);
			fprintf(out,"\t\t<PHOTOS_LINK>%s</PHOTOS_LINK>\r\n",r->photos_link);
			fprintf(out,"</PRODUCT>\r\n");
		}
		fprintf(out,"</RECALLS_DATA>\r\n");
		fclose(out);	
	}

	/* Exists for reading, close it and report error. */
	else{
		fclose(out);
		printf("File exists.  Use \'overwrite <filename>\' instead.\n");
	}
}

void overwriteRecords(char* filename, vector* v){
	FILE* out;
	out = fopen(filename,"r");

	/* Exists for reading, close it and re-open it for writing. */
	if(out){
		fclose(out);
		out = fopen(filename,"w");
		fprintf(out,"<?xml version=\"1.0\" encoding=\"UTF-8\" ?>\r\n");
		fprintf(out,"<RECALLS_DATA>\r\n");
		for(int i=0;i<VEC_SIZE(*v);i++){
			fprintf(out,"<PRODUCT>\r\n");
			struct record* r = VEC_GET(*v,i,struct record*);
			fprintf(out,"\t\t<DATE>%s</DATE>\r\n",r->date);		
			fprintf(out,"\t\t<BRAND_NAME><![CDATA[%s]]></BRAND_NAME>\r\n",r->brand_name);
			fprintf(out,"\t\t<PRODUCT_DESCRIPTION><![CDATA[%s]]></PRODUCT_DESCRIPTION>\r\n",r->product_description);
			fprintf(out,"\t\t<REASON><![CDATA[%s]]></REASON>\r\n",r->reason);
			fprintf(out,"\t\t<COMPANY><![CDATA[%s]]></COMPANY>\r\n",r->company);
			fprintf(out,"\t\t<COMPANY_RELEASE_LINK>%s</COMPANY_RELEASE_LINK>\r\n",r->company_release);
			fprintf(out,"\t\t<PHOTOS_LINK>%s</PHOTOS_LINK>\r\n",r->photos_link);
			fprintf(out,"</PRODUCT>\r\n");
		}
		fprintf(out,"</RECALLS_DATA>\r\n");
		fclose(out);	
	}

	/* Doesn't exist for reading.  Report error. */
	else{
		printf("File doesn't exist.  Use \'save <filename>\' instead.\n");
	}
}

struct field* stripTags(char* input){
	struct field* result = NULL;
	const char delim[3] = "<>";
	char* inputcopy = strdup(input);
	char* token;
	char* text;
	int fieldtype = -1;
	token = strtok(inputcopy, delim);
	if(strcmp(token,"\t")==0){token = strtok(NULL,delim);}
	if(strcmp(token,"DATE")==0){
		fieldtype = DATE;
		token = strtok(NULL,delim);
	}
	else if(strcmp(token,"REASON")==0){
		fieldtype = REASON;
		token = strtok(NULL,delim);
	}
	else if(strcmp(token,"BRAND_NAME")==0){
		fieldtype = BRAND_NAME;
		token = strtok(NULL,delim);
	}
	else if(strcmp(token,"PRODUCT_DESCRIPTION")==0){
		fieldtype = PRODUCT_DESCRIPTION;
		token = strtok(NULL,delim);
	}
	else if(strcmp(token,"COMPANY")==0){
		fieldtype = COMPANY;
		token = strtok(NULL,delim);
	}
	else if(strcmp(token,"COMPANY_RELEASE_LINK")==0){
		fieldtype = COMPANY_RELEASE;
		token = strtok(NULL,delim);
	}
	else if(strcmp(token,"PHOTOS_LINK")==0){
		fieldtype = PHOTOS_LINK;
		token = strtok(NULL,delim);
	}
	if(fieldtype!=-1){
		//const char delim2[3] = "[]";		
		//char* tokencopy = strdup(token);
		//token2 = strtok(tokencopy,delim2);
		//token2 = strtok(tokencopy,NULL);
		//token2 = strtok(tokencopy,NULL);
		//text = strdup(token2);
		text = strdup(token);
		char* cleanedtext = stripInnerTags(text);
//		if(text!=cleanedtext){free(text);}
		result = new_field(fieldtype, cleanedtext);
		//free(tokencopy);
		if(text!=cleanedtext){free(cleanedtext);}
		free(text);
	}
	free(inputcopy);
	return result;
}

char* stripInnerTags(char* input){
	char* text;
	const char delim[3] = "[]";
	char* inputcopy = strdup(input);
	char* token;
	token = strtok(inputcopy,delim);
	token = strtok(NULL,delim);
	token = strtok(NULL,delim);
	if(token){
		//free(inputcopy);
		text = strdup(token);
		free(inputcopy);
		return text;
	}
	free(inputcopy);
	return input;
}

char* getField(struct record* r,int tag){
	switch(tag){
		case DATE:{return r->date;}
		case REASON:{return r->reason;}
		case BRAND_NAME:{return r->brand_name;}
		case PRODUCT_DESCRIPTION:{return r->product_description;}
		case COMPANY:{return r->company;}
		case COMPANY_RELEASE:{return r->company_release;}
		case PHOTOS_LINK:{return r->photos_link;}
	}
	return NULL;
}

void addFieldtoRecord(struct field* f, struct record* r){
	int tag = f->tag;
	char* text = f->text;
	switch(tag){
		case DATE:{r->date = text;break;}
		case REASON:{r->reason = text;break;}
		case BRAND_NAME:{r->brand_name = text;break;}
		case PRODUCT_DESCRIPTION:{r->product_description = text;break;}
		case COMPANY:{r->company = text;break;}
		case COMPANY_RELEASE:{r->company_release = text;break;}
		case PHOTOS_LINK:{r->photos_link = text;break;}
	}
}
/*This is just for visually verifying records have been parsed and added properly.*/
void printRecords(vector* v){
	for(int i = 0; i < VEC_SIZE(*v); i++){
		struct record* r = VEC_GET(*v, i, struct record*);
		printf("%d:\n",i);
		if(r->date){printf("\t%s\n",r->date);}
		if(r->brand_name){printf("\t%s\n",r->brand_name);}
		if(r->product_description){printf("\t%s\n",r->product_description);}
		if(r->reason){printf("\t%s\n",r->reason);}
		if(r->company){printf("\t%s\n",r->company);}
		if(r->company_release){printf("\t%s\n",r->company_release);}
		if(r->photos_link){printf("\t%s\n",r->photos_link);}	
	}
}

void printHelpCommands(){
	printf("Available Commands:\n");
	printf("filter <keyword> in <field>\n");
	printf("filter <keyword> notin <field>\n");
	printf("filter <keyword> in <field> OR <additional filter>\n");
	printf("filter <keyword> notin <field> OR <additional filter>\n");
	printf("reset\n");
	printf("save <filename>\n");
	printf("overwrite <filename>\n");
	printf("count\n");
	printf("print\n");
	printf("help\n");
	printf("quit\n");
}

int ifInputValid(char* input){
	
        if(input[0]==' ')		return 0;
	
	if(strcmp(input,"help\n")==0)	return 1;	

        if(strcmp(input,"quit\n")==0)	return 1;

        if(strcmp(input,"reset\n")==0)	return 1;

        if(strcmp(input,"count\n")==0)	return 1;

        if(strcmp(input,"print\n")==0)	return 1;
	
	else{
		const char s[2] = " ";
                char *token;
                int term = 0 ;
                token = strtok(input,s);

		if(strcmp(token ,"save")==0){
			while(token!=NULL){
                        	if(term==0){
                                	if(strlen(token)!=4)    return 0;
                        	}
                        	/*if(term==1){
                                	if(strlen(token)<4)             return 0;
                                	if(token[0]!='"')               return 0;
                                	if(token[strlen(token)-2]!='"') return 0;
                        	}*/
                        	term++;
                        	token = strtok(NULL,s);
                	}
                	if(term==2)     return 1;
                	else            return 0;
		}
		else if(strcmp(token ,"filter")==0){
			while(token!=NULL){
                        	if(term%5==0){
                                	if(strlen(token)!=6)    	return 0;
					if(strcmp(token,"filter")!=0)	return 0;
                        	}
                        	if(term%5==1){
                                	if(strlen(token)<2)             return 0;
                                	if(token[0]!='"')               return 0;
                                	if(token[strlen(token)-1]!='"') return 0;
                        	}
                        	if(term%5==2){
					if(strcmp(token,"in")==0){}		
					else if(strcmp(token,"notin")==0){}
					else {return 0;}
                        	}
                        	if(term%5==3){

                        	}
                        	if(term%5==4){
					//if(strcmp(token,"AND")==0){}
					if(strcmp(token,"OR")==0){}
					else {return 0;}
                        	}
                        	term++;
                        	token = strtok(NULL,s);
                	}
                	if(term%5==4)   return 1;
                	else            return 0;
		}
		else if(strcmp(token ,"overwrite")==0){
			while(token!=NULL){
                        	if(term==0){
                                	if(strlen(token)!=9)    return 0;
                        	}
                        	/*if(term==1){
                                	if(strlen(token)<4)             return 0;
                                	if(token[0]!='"')               return 0;
                                	if(token[strlen(token)-2]!='"') return 0;
                        	}*/
                        	term++;
                        	token = strtok(NULL,s);
                	}
                	if(term==2)     return 1;
                	else            return 0;
		}
		else{
			return 0;
		}
	}
	return 0;
}







//*************** Filtering Methods Begin Here ********************

vector* filterRecords(vector* v, char** keyList, char** typeList, char** fieldList, int listSize){
	vector* filteredRecords = malloc(sizeof(vector));
	vector_init(filteredRecords);
	for(int i = 0; i < VEC_SIZE(*v); i++){	//Loop over records
                struct record* r = VEC_GET(*v, i, struct record*);
		for(int index = 0; index <listSize; index++){	//Loop over filters
			char* keyword = keyList[index];
			char* type = typeList[index];
			char* f = fieldList[index];

			//This code strips off the parentheses around keyword
			char trueKey[sizeof(keyword)-1];
        		strcpy(trueKey, &keyword[1]);
        		trueKey[strlen(trueKey)-1] = 0;

			if(strcmp(f,"DATE\n")==0 || strcmp(f,"DATE")==0){
				char* fieldText = r->date;
				if(strstr(fieldText, trueKey) != NULL && strcmp(type,"in") == 0){
					VEC_ADD(*filteredRecords,r);
					index = listSize; //Stops filter loop since record already added
				}
				else if(strstr(fieldText, trueKey) == NULL && strcmp(type,"notin") == 0){
					VEC_ADD(*filteredRecords,r);
					index = listSize;
				}
			}
			else if(strcmp(f,"BRAND_NAME\n")==0 || strcmp(f,"BRAND_NAME")==0){
                        	char* fieldText = r->brand_name;
                        	if(strstr(fieldText, trueKey) != NULL && strcmp(type,"in") == 0){
                                	VEC_ADD(*filteredRecords,r);
					index = listSize;
                        	}
				else if(strstr(fieldText, trueKey) == NULL && strcmp(type,"notin") == 0){
                                        VEC_ADD(*filteredRecords,r);
                                        index = listSize;
                                }
                	}
			else if(strcmp(f,"PRODUCT_DESCRIPTION\n")==0 || strcmp(f,"PRODUCT_DESCRIPTION")==0){
                        	char* fieldText = r->product_description;
                        	if(strstr(fieldText, trueKey) != NULL && strcmp(type,"in") == 0){
					VEC_ADD(*filteredRecords,r);
					index = listSize;
                        	}
				else if(strstr(fieldText, trueKey) == NULL && strcmp(type,"notin") == 0){
                                        VEC_ADD(*filteredRecords,r);
                                        index = listSize;
                                }
                	}
			else if(strcmp(f,"REASON\n")==0 || strcmp(f,"REASON")==0){
                        	char* fieldText = r->reason;
                        	if(strstr(fieldText, trueKey) != NULL && strcmp(type,"in") == 0){
					VEC_ADD(*filteredRecords,r);
					index = listSize;
                        	}
				else if(strstr(fieldText, trueKey) == NULL && strcmp(type,"notin") == 0){
                                        VEC_ADD(*filteredRecords,r);
                                        index = listSize;
                                }
                	}
			else if(strcmp(f,"COMPANY\n")==0 || strcmp(f,"COMPANY")==0){
                        	char* fieldText = r->company;
                        	if(strstr(fieldText, trueKey) != NULL && strcmp(type,"in") == 0){
                                	VEC_ADD(*filteredRecords,r);
					index = listSize;
                        	}
				else if(strstr(fieldText, trueKey) == NULL && strcmp(type,"notin") == 0){
                                        VEC_ADD(*filteredRecords,r);
                                        index = listSize;
                                }
                	}
			else if(strcmp(f,"COMPANY_RELEASE\n")==0 || strcmp(f,"COMPANY_RELEASE")==0){
                        	char* fieldText = r->company_release;
                        	if(strstr(fieldText, trueKey) != NULL && strcmp(type,"in") == 0){
                                	VEC_ADD(*filteredRecords,r);
					index = listSize;
                        	}
				else if(strstr(fieldText, trueKey) == NULL && strcmp(type,"notin") == 0){
                                        VEC_ADD(*filteredRecords,r);
                                        index = listSize;
                                }
                	}
			else if(strcmp(f,"PHOTOS_LINK\n")==0 || strcmp(f,"PHOTOS_LINK")==0){
                        	char* fieldText = r->photos_link;
                        	if(strstr(fieldText, trueKey) != NULL && strcmp(type,"in") == 0){
                                	VEC_ADD(*filteredRecords,r);
					index = listSize;
                        	}
				else if(strstr(fieldText, trueKey) == NULL && strcmp(type,"notin") == 0){
                                        VEC_ADD(*filteredRecords,r);
                                        index = listSize;
                                }
                	}
			else if(strcmp(f,"ANY\n")==0 || strcmp(f,"ANY")==0){
				if(strcmp(type,"in") == 0){
					int didAnyMatch = 0;
					char* fieldText = r->date;
                        		if(strstr(fieldText, trueKey) != NULL){didAnyMatch = 1;}
					fieldText = r->brand_name;
					if(strstr(fieldText, trueKey) != NULL){didAnyMatch = 1;}
					fieldText = r->product_description;
                        		if(strstr(fieldText, trueKey) != NULL){didAnyMatch = 1;}
					fieldText = r->reason;
					if(strstr(fieldText, trueKey) != NULL){didAnyMatch = 1;}
					fieldText = r->company;
					if(strstr(fieldText, trueKey) != NULL){didAnyMatch = 1;}
					fieldText = r->company_release;
					if(strstr(fieldText, trueKey) != NULL){didAnyMatch = 1;}
					fieldText = r->photos_link;
					if(strstr(fieldText, trueKey) != NULL){didAnyMatch = 1;}

					if(didAnyMatch == 1){
						VEC_ADD(*filteredRecords,r);
					}
				}
				else if(strcmp(type,"notin") == 0){
					int didAnyMatch = 1;
                                        char* fieldText = r->date;
                                        if(strstr(fieldText, trueKey) != NULL){didAnyMatch = 0;}
                                        fieldText = r->brand_name;
                                        if(strstr(fieldText, trueKey) != NULL){didAnyMatch = 0;}
                                        fieldText = r->product_description;
                                        if(strstr(fieldText, trueKey) != NULL){didAnyMatch = 0;}
                                        fieldText = r->reason;
                                        if(strstr(fieldText, trueKey) != NULL){didAnyMatch = 0;}
                                        fieldText = r->company;
                                        if(strstr(fieldText, trueKey) != NULL){didAnyMatch = 0;}
                                        fieldText = r->company_release;
                                        if(strstr(fieldText, trueKey) != NULL){didAnyMatch = 0;}
                                        fieldText = r->photos_link;
                                        if(strstr(fieldText, trueKey) != NULL){didAnyMatch = 0;}

                                        if(didAnyMatch == 1){
                                                VEC_ADD(*filteredRecords,r);
                                        }
				}
			}
			else if(strcmp(f,"ALL\n")==0 || strcmp(f,"ALL")==0){
				if(strcmp(type,"in") == 0){
					int didAllMatch = 1;
					char* fieldText = r->date;
					if(strstr(fieldText, trueKey) == NULL){didAllMatch = 0;}
					fieldText = r->brand_name;
					if(strstr(fieldText, trueKey) == NULL){didAllMatch = 0;}
					fieldText = r->product_description;
					if(strstr(fieldText, trueKey) == NULL){didAllMatch = 0;}
					fieldText = r->reason;
					if(strstr(fieldText, trueKey) == NULL){didAllMatch = 0;}
					fieldText = r->company;
					if(strstr(fieldText, trueKey) == NULL){didAllMatch = 0;}
					fieldText = r->company_release;
					if(strstr(fieldText, trueKey) == NULL){didAllMatch = 0;}
					fieldText = r->photos_link;
					if(strstr(fieldText, trueKey) == NULL){didAllMatch = 0;}

					if(didAllMatch == 1){
						VEC_ADD(*filteredRecords,r);
					}
				}
				if(strcmp(type,"notin") == 0){
                                        int didAllMatch = 0;
                                        char* fieldText = r->date;
                                        if(strstr(fieldText, trueKey) == NULL){didAllMatch = 1;}
                                        fieldText = r->brand_name;
                                        if(strstr(fieldText, trueKey) == NULL){didAllMatch = 1;}
                                        fieldText = r->product_description;
                                        if(strstr(fieldText, trueKey) == NULL){didAllMatch = 1;}
                                        fieldText = r->reason;
                                        if(strstr(fieldText, trueKey) == NULL){didAllMatch = 1;}
                                        fieldText = r->company;
                                        if(strstr(fieldText, trueKey) == NULL){didAllMatch = 1;}
                                        fieldText = r->company_release;
                                        if(strstr(fieldText, trueKey) == NULL){didAllMatch = 1;}
                                        fieldText = r->photos_link;
                                        if(strstr(fieldText, trueKey) == NULL){didAllMatch = 1;}

                                        if(didAllMatch == 1){
                                                VEC_ADD(*filteredRecords,r);
                                        }
                                }
			}
		}
	}
	return filteredRecords;
}

