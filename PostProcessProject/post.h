#ifndef POST
#define POST
#endif
#include "vector.h"

#define DATE 0
#define BRAND_NAME 1
#define PRODUCT_DESCRIPTION 2
#define REASON 3
#define COMPANY 4
#define COMPANY_RELEASE 5
#define PHOTOS_LINK 6

struct record{
	char* date;
	char* brand_name;
	char* product_description;
	char* reason;
	char* company;
	char* company_release;
	char* photos_link;
};

struct field{
	int tag;
	char* text;	
};
int readInputFile(char*, vector*);

struct record* new_record();
struct field* new_field(int,char*);

void destroyRecord(struct record*);
void freeRecords(vector*);
void saveRecords(char*, vector* v);
void overwriteRecords(char*, vector* v);
/* Strips xml tags from a line of input, returns a char* to the string between the tags. */
struct field* stripTags(char*);

char* stripInnerTags(char*);

char* getField(struct record*, int);

void addFieldtoRecord(struct field*, struct record*);

void printRecords(vector*);

vector* filterRecords(vector*, char**, char**, char**, int);

void printHelpCommands();

int ifInputValid(char*);
