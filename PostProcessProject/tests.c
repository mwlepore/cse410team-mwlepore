#include <stdio.h>
#include <string.h>
#include <stdbool.h>
#include <stdlib.h>
#include "CUnit.h"
#include "Basic.h"

#ifndef POST
#define POST
#include "post.h"
#endif

#ifndef VECTOR
#define VECTOR
#include "vector.h"
#endif


void openFileTest(char* filename, int expected){
	VEC_INIT(v);	
	int actual = readInputFile(filename,&v);
	CU_ASSERT_EQUAL(expected,actual);
	freeRecords(&v);
	VEC_FREE(v);
}

void numberOfRecordsTest(char* filename, int expected){
	VEC_INIT(v);
	int actual = readInputFile(filename,&v);
	CU_ASSERT_EQUAL(expected,actual);
	freeRecords(&v);
	VEC_FREE(v);
}

void vectorinitTest(bool expected){
	VEC_INIT(v);
	int size = VEC_SIZE(v);
	int cap = (&v)->capacity;
	bool actual = cap==4&&size==0;
	CU_ASSERT_EQUAL(expected,actual);
	VEC_FREE(v);
}
void vectorSizeTest(int expected){
	VEC_INIT(v);
	for(long i = 0;i<expected;i++){
		VEC_ADD(v,(long*)i);
	}
	int actual = VEC_SIZE(v);
	CU_ASSERT_EQUAL(expected,actual);
	free(v.items);
	//VEC_FREE(v);
}
void vectorResizeTest(int items, int expected){
	VEC_INIT(v);
	for(long i = 0;i<items;i++){
		VEC_ADD(v,(long*)i);
	}
	int actual = (&v)->capacity;
	CU_ASSERT_EQUAL(expected,actual);
	free(v.items);
	//VEC_FREE(v);
}
void vectorSetup(vector* vec){
	//vector v = *vec;
	//CU_ASSERT_EQUAL(VEC_SIZE(v),0);
	char* a = "Matt";
	char* b = "Torri";
	char* c = "John";
	char* d = "Mark";
	VEC_ADD((*vec), a);
	VEC_ADD((*vec), b);
	VEC_ADD((*vec), c);
	VEC_ADD((*vec), d);	
}
void vectorGetTest(int index, char* expected){
	VEC_INIT(v);
	vectorSetup(&v);
	char* actual = VEC_GET(v,index,char*);
	CU_ASSERT_EQUAL(strcmp(actual,expected),0);
	VEC_FREE(v);
}
void vectorReplaceTest(int index, char* expected){
	VEC_INIT(v);
	vectorSetup(&v);
	VEC_REPLACE(v,index,expected);
	char* actual = VEC_GET(v,index,char*);
	CU_ASSERT_EQUAL(strcmp(actual,expected),0);
	VEC_FREE(v);
}
void vectorOutofBoundsTest(void* expected){
	VEC_INIT(v);
	vectorSetup(&v);
	void* actual = VEC_GET(v,VEC_SIZE(v)+1,void*);
	CU_ASSERT_EQUAL(actual,expected);
	VEC_FREE(v);
}

void recordinitTest(bool expected){
	struct record* r = new_record();
	if(r){
		bool actual = (
				r->date==NULL&&
				r->brand_name==NULL&&
				r->product_description==NULL&&
				r->reason==NULL&&
				r->company==NULL&&
				r->company_release==NULL&&
				r->photos_link==NULL
		);
		CU_ASSERT_EQUAL(actual,expected);
	}
	free(r);
}

void stripTest(char* input, char* expected){
	struct field* x = stripTags(input);
	if(expected){
		char* actual = x->text;
		CU_ASSERT_EQUAL(strcmp(actual,expected),0);
		free(x->text);
		free(x);
	}
	else{
		CU_ASSERT_EQUAL((void*)x,(void*)expected);
		free(x);
	}
	//free(x->text);
	//free(x);
}


void stripInnerTest(char* input, char* expected){
	char* actual = stripInnerTags(input);
	CU_ASSERT_EQUAL(strcmp(actual,expected),0);
	free(actual);
}

void fullStripTest(char* input, char* expected){
	struct field* f = stripTags(input);
	char* actual = f->text;
	CU_ASSERT_EQUAL(strcmp(actual,expected),0);
	free(f->text);
	free(f);
}

void fieldTest(int tag, char* expected){
	struct record* r = new_record();
	struct field* f = new_field(tag,expected);
	addFieldtoRecord(f,r);
	char* actual = getField(r,tag);
	CU_ASSERT_EQUAL(strcmp(actual,expected),0);
	free(f->text);
	free(f);
	free(r);
}

void saveTest(char* filename,bool expected){
	VEC_INIT(v);
	FILE* test;
	test = fopen(filename,"r");
	bool b = test==NULL;
	saveRecords(filename, &v);
	test = fopen(filename,"r");
	bool b1 = test!=NULL;
	bool actual = b&&b1;
	if(test){fclose(test);}
	CU_ASSERT_EQUAL(actual, expected);
	VEC_FREE(v);
}

void overwriteTest(char* filename, bool expected){
	VEC_INIT(v);
	FILE* test;
	test = fopen(filename,"r");
	bool b = test==NULL;
	if(test){fclose(test);}
	overwriteRecords(filename, &v);
	test = fopen(filename,"r");
	bool b1 = test==NULL;
	bool actual = b==b1;
	if(test){fclose(test);}
	CU_ASSERT_EQUAL(actual,expected);
	VEC_FREE(v);
}
//Filtering Tests Begin Here

struct record* generateTestRecord(char* d, char * bn, char* pd, char* r, char* c, char * crl, char* pl){
	struct record* testRecord = new_record();

	struct field* f1 = new_field(DATE, d);
	struct field* f2 = new_field(BRAND_NAME, bn);
	struct field* f3 = new_field(PRODUCT_DESCRIPTION, pd);
	struct field* f4 = new_field(REASON, r);
	struct field* f5 = new_field(COMPANY, c);
	struct field* f6 = new_field(COMPANY_RELEASE, crl);
	struct field* f7 = new_field(PHOTOS_LINK, pl);

	addFieldtoRecord(f1, testRecord);
	addFieldtoRecord(f2, testRecord);
	addFieldtoRecord(f3, testRecord);
	addFieldtoRecord(f4, testRecord);
	addFieldtoRecord(f5, testRecord);
	addFieldtoRecord(f6, testRecord);
	addFieldtoRecord(f7, testRecord);

	free(f1);
	free(f2);
	free(f3);
	free(f4);
	free(f5);
	free(f6);
	free(f7);
	return testRecord;
}

int compareVectors(vector* v1, vector* v2){
	if(VEC_SIZE(*v1) != VEC_SIZE(*v2)){
		return 0;
	}
	int isMatch = 1;
	for(int i = 0; i < VEC_SIZE(*v1); i++){
		if(isMatch == 1){
			struct record* r1 = VEC_GET(*v1, i, struct record*);

			char* f1 = r1->date;
			char* f2 = r1->brand_name;
			char* f3 = r1->product_description;
			char* f4 = r1->reason;
			char* f5 = r1->company;
			char* f6 = r1->company_release;
			char* f7 = r1->photos_link;

			isMatch = 0;

			for(int x = 0; x < VEC_SIZE(*v2); x++){
				struct record* r2 = VEC_GET(*v2, x, struct record*);
				
				char* fa = r2->date;
				char* fb = r2->brand_name;
				char* fc = r2->product_description;
				char* fd = r2->reason;
				char* fe = r2->company;
				char* ff = r2->company_release;
				char* fg = r2->photos_link;

				if(strcmp(f1,fa)==0 && strcmp(f2,fb)==0 && strcmp(f3,fc)==0 && strcmp(f4,fd)==0 && strcmp(f5,fe)==0 && strcmp(f6,ff)==0 && strcmp(f7,fg)==0){
					isMatch = 1;
					x = VEC_SIZE(*v2);
				}
				
			}
		}
		else{
			return 0;
		}
	}
	return 1;
}

void inFilterTest1(char* inputFile){
	VEC_INIT(records);
        readInputFile(inputFile, &records);
        vector* newRecords = &records;

        char *keyList[3];
        char *typeList[3];
        char *fieldList[3];
        int listIndex = 1;
        keyList[0] = "\"contaminated\"";
        typeList[0] = "in";
        fieldList[0] = "REASON\n";
        newRecords = filterRecords(newRecords, keyList, typeList, fieldList, listIndex);

        vector* expected = malloc(sizeof(vector));
        vector_init(expected);
	struct record* r1 = generateTestRecord("Wed, 26 Apr 2017 17:00:00 -0400","Farming Fish","Organic Basil Pesto","May be contaminated with Listeria monocytogenes.","Local Bounty","https://www.fda.gov/Safety/Recalls/ucm555399.htm","c");
	struct record* r2 = generateTestRecord("Wed, 26 Apr 2017 00:00:00 -0400","Wegmans","O�Brien frozen hash browns","May be contaminated with extraneous golf ball materials.","McCain Foods USA, Inc.","https://www.fda.gov/Safety/Recalls/ucm555385.htm","d");
	VEC_ADD(*expected, r1);
	VEC_ADD(*expected, r2);

        CU_ASSERT_EQUAL(compareVectors(newRecords,expected),1);
	freeRecords(&records);
	freeRecords(expected);
	VEC_FREE(records);
	VEC_FREE(*expected);
	free(expected);
}

void inFilterTest2(char* inputFile){
        VEC_INIT(records);
        readInputFile(inputFile, &records);
        vector* newRecords = &records;

        char *keyList1[3], *keyList2[3], *keyList3[3], *keyList4[3];
        char *typeList1[3], *typeList2[3], *typeList3[3], *typeList4[3];
        char *fieldList1[3], *fieldList2[3], *fieldList3[3], *fieldList4[3];
        int listIndex = 1;
        keyList1[0] = "\"2017\"";
        typeList1[0] = "in";
        fieldList1[0] = "DATE\n";
        keyList2[0] = "\"Beef\"";
        typeList2[0] = "in";
        fieldList2[0] = "PRODUCT_DESCRIPTION\n";
        keyList3[0] = "\"www\"";
        typeList3[0] = "in";
        fieldList3[0] = "COMPANY_RELEASE\n";
	keyList4[0] = "\"f\"";
        typeList4[0] = "in";
        fieldList4[0] = "PHOTOS_LINK\n";

        newRecords = filterRecords(newRecords, keyList1, typeList1, fieldList1, listIndex);
        newRecords = filterRecords(newRecords, keyList2, typeList2, fieldList2, listIndex);
        newRecords = filterRecords(newRecords, keyList3, typeList3, fieldList3, listIndex);
	newRecords = filterRecords(newRecords, keyList4, typeList4, fieldList4, listIndex);

        vector* expected = malloc(sizeof(vector));
        vector_init(expected);
        struct record* r1 = generateTestRecord("Mon, 24 Apr 2017 19:43:00 -0400","Cocolicious","Beef &amp; Turkey Dog Food","Presence of pentobarbital","Party Animal","https://www.fda.gov/Safety/Recalls/ucm554771.htm","f");
        VEC_ADD(*expected, r1);

        CU_ASSERT_EQUAL(compareVectors(newRecords,expected),1);
        VEC_FREE(records);
        free(expected);
}

void notInFilterTest1(char* inputFile){
	VEC_INIT(records);
        readInputFile(inputFile, &records);
        vector* newRecords = &records;

        char *keyList1[3], *keyList2[3], *keyList3[3];
        char *typeList1[3], *typeList2[3], *typeList3[3];
        char *fieldList1[3], *fieldList2[3], *fieldList3[3];
        int listIndex = 1;
        keyList1[0] = "\"contaminated\"";
        typeList1[0] = "notin";
        fieldList1[0] = "REASON\n";
	keyList2[0] = "\"Organic\"";
        typeList2[0] = "notin";
        fieldList2[0] = "BRAND_NAME\n";
	keyList3[0] = "\"Apr\"";
        typeList3[0] = "notin";
        fieldList3[0] = "DATE\n";
	
        newRecords = filterRecords(newRecords, keyList1, typeList1, fieldList1, listIndex);
	newRecords = filterRecords(newRecords, keyList2, typeList2, fieldList2, listIndex);
	newRecords = filterRecords(newRecords, keyList3, typeList3, fieldList3, listIndex);

        vector* expected = malloc(sizeof(vector));
        vector_init(expected);
	struct record* r1 = generateTestRecord("a","a","a","a","a","a","a");
	VEC_ADD(*expected, r1);

        CU_ASSERT_EQUAL(compareVectors(newRecords,expected),1);
	freeRecords(&records);
	freeRecords(expected);
        VEC_FREE(records);
	VEC_FREE(*expected);
        free(expected);
}

void notInFilterTest2(char* inputFile){
        VEC_INIT(records);
        readInputFile(inputFile, &records);
        vector* newRecords = &records;

        char *keyList1[3], *keyList2[3], *keyList3[3], *keyList4[3];
        char *typeList1[3], *typeList2[3], *typeList3[3], *typeList4[3];
        char *fieldList1[3], *fieldList2[3], *fieldList3[3], *fieldList4[3];
        int listIndex = 1;
        keyList1[0] = "\"www\"";
        typeList1[0] = "notin";
        fieldList1[0] = "COMPANY_RELEASE\n";
        keyList2[0] = "\"Animal\"";
        typeList2[0] = "notin";
        fieldList2[0] = "COMPANY\n";
        keyList3[0] = "\"1.8\"";
        typeList3[0] = "notin";
        fieldList3[0] = "PRODUCT_DESCRIPTION\n";
	keyList4[0] = "\"red\"";
        typeList4[0] = "notin";
        fieldList4[0] = "PHOTOS_LINK\n";

        newRecords = filterRecords(newRecords, keyList1, typeList1, fieldList1, listIndex);
        newRecords = filterRecords(newRecords, keyList2, typeList2, fieldList2, listIndex);
        newRecords = filterRecords(newRecords, keyList3, typeList3, fieldList3, listIndex);
	newRecords = filterRecords(newRecords, keyList4, typeList4, fieldList4, listIndex);

        vector* expected = malloc(sizeof(vector));
        vector_init(expected);
        struct record* r1 = generateTestRecord("a","a","a","a","a","a","a");
        VEC_ADD(*expected, r1);

        CU_ASSERT_EQUAL(compareVectors(newRecords,expected),1);
        VEC_FREE(records);
        free(expected);
}

void allFilterTest1(char* inputFile){
        VEC_INIT(records);
	readInputFile(inputFile, &records);
        vector* newRecords = &records;

	char *keyList[3];
        char *typeList[3];
        char *fieldList[3];
        int listIndex = 1;
	keyList[0] = "\"a\"";
	typeList[0] = "in";
	fieldList[0] = "ALL\n";
	newRecords = filterRecords(newRecords, keyList, typeList, fieldList, listIndex);

        vector* expected = malloc(sizeof(vector));
        vector_init(expected);
	struct record* r1 = generateTestRecord("a","a","a","a","a","a","a");
        VEC_ADD(*expected, r1);

        CU_ASSERT_EQUAL(compareVectors(newRecords,expected),1);
	freeRecords(&records);
	freeRecords(expected);
	VEC_FREE(records);
	VEC_FREE(*expected);
	free(expected);
}

void allFilterTest2(char* inputFile){
	VEC_INIT(records);
        readInputFile(inputFile, &records);
        vector* newRecords = &records;

        char *keyList[3], *keyList2[3];
        char *typeList[3], *typeList2[3];;
        char *fieldList[3], *fieldList2[3];
        int listIndex = 1;
        keyList[0] = "\"a\"";
        typeList[0] = "notin";
        fieldList[0] = "ALL\n";
	keyList2[0] = "\"d\"";
        typeList2[0] = "notin";
        fieldList2[0] = "PHOTOS_LINK\n";
        newRecords = filterRecords(newRecords, keyList, typeList, fieldList, listIndex);
	newRecords = filterRecords(newRecords, keyList2, typeList2, fieldList2, listIndex);

        vector* expected = malloc(sizeof(vector));
        vector_init(expected);
        struct record* r1 = generateTestRecord("Thu, 27 Apr 2017 00:00:00 -0400","Organic Veda","Ginger Powder","Salmonella","Lord Organics","https://www.fda.gov/Safety/Recalls/ucm555723.htm","b");
        struct record* r2 = generateTestRecord("Wed, 26 Apr 2017 17:00:00 -0400","Farming Fish","Organic Basil Pesto","May be contaminated with Listeria monocytogenes.","Local Bounty","https://www.fda.gov/Safety/Recalls/ucm555399.htm","c");
        struct record* r4 = generateTestRecord("Wed, 26 Apr 2017 00:00:00 -0400","Homemade Gourmet","Dip Trio Mix","Salmonella","Phoenix Food, LLC","https://www.fda.gov/Safety/Recalls/ucm555645.htm","e");
        struct record* r5 = generateTestRecord("Mon, 24 Apr 2017 19:43:00 -0400","Cocolicious","Beef &amp; Turkey Dog Food","Presence of pentobarbital","Party Animal","https://www.fda.gov/Safety/Recalls/ucm554771.htm","f");
        struct record* r6 = generateTestRecord("Mon, 24 Apr 2017 08:07:00 -0400","Soylent","1.8 Powder Bounty","Undeclared Milk","Soylent","https://www.fda.gov/Safety/Recalls/ucm554489.htm","g");
        VEC_ADD(*expected, r1);
        VEC_ADD(*expected, r2);
        //VEC_ADD(*expected, r3);
        VEC_ADD(*expected, r4);
        VEC_ADD(*expected, r5);
        VEC_ADD(*expected, r6);

        CU_ASSERT_EQUAL(compareVectors(newRecords,expected),1);
        VEC_FREE(records);
        free(expected);
	
}

void anyFilterTest1(char* inputFile){
        VEC_INIT(records);
        readInputFile(inputFile, &records);
        vector* newRecords = &records;

        char *keyList[3];
        char *typeList[3];
        char *fieldList[3];
        int listIndex = 1;
        keyList[0] = "\"Bounty\"";
        typeList[0] = "in";
        fieldList[0] = "ANY\n";
        newRecords = filterRecords(newRecords, keyList, typeList, fieldList, listIndex);

        vector* expected = malloc(sizeof(vector));
        vector_init(expected);
	struct record* r1 = generateTestRecord("Wed, 26 Apr 2017 17:00:00 -0400","Farming Fish","Organic Basil Pesto","May be contaminated with Listeria monocytogenes.","Local Bounty","https://www.fda.gov/Safety/Recalls/ucm555399.htm","c");
        struct record* r2 = generateTestRecord("Mon, 24 Apr 2017 08:07:00 -0400","Soylent","1.8 Powder Bounty","Undeclared Milk","Soylent","https://www.fda.gov/Safety/Recalls/ucm554489.htm","g");
        VEC_ADD(*expected, r1);
	VEC_ADD(*expected, r2);

        CU_ASSERT_EQUAL(compareVectors(newRecords,expected),1);
	freeRecords(&records);
	freeRecords(expected);
        VEC_FREE(records);
	VEC_FREE(*expected);
        free(expected);
}

void anyFilterTest2(char* inputFile){
        VEC_INIT(records);
        readInputFile(inputFile, &records);
        vector* newRecords = &records;

        char *keyList[3];
        char *typeList[3];
        char *fieldList[3];
        int listIndex = 1;
        keyList[0] = "\"We\"";
        typeList[0] = "notin";
        fieldList[0] = "ANY\n";
        newRecords = filterRecords(newRecords, keyList, typeList, fieldList, listIndex);

        vector* expected = malloc(sizeof(vector));
        vector_init(expected);
	struct record* r1 = generateTestRecord("a","a","a","a","a","a","a");
	struct record* r2 = generateTestRecord("Thu, 27 Apr 2017 00:00:00 -0400","Organic Veda","Ginger Powder","Salmonella","Lord Organics","https://www.fda.gov/Safety/Recalls/ucm555723.htm","b");
        struct record* r3 = generateTestRecord("Mon, 24 Apr 2017 19:43:00 -0400","Cocolicious","Beef &amp; Turkey Dog Food","Presence of pentobarbital","Party Animal","https://www.fda.gov/Safety/Recalls/ucm554771.htm","f");
	struct record* r4 = generateTestRecord("Mon, 24 Apr 2017 08:07:00 -0400","Soylent","1.8 Powder Bounty","Undeclared Milk","Soylent","https://www.fda.gov/Safety/Recalls/ucm554489.htm","g");
        VEC_ADD(*expected, r1);
        VEC_ADD(*expected, r2);
	VEC_ADD(*expected, r3);
	VEC_ADD(*expected, r4);

        CU_ASSERT_EQUAL(compareVectors(newRecords,expected),1);
        VEC_FREE(records);
        free(expected);
}

void orFilterTest1(char* inputFile){
        VEC_INIT(records);
        readInputFile(inputFile, &records);
        vector* newRecords = &records;

        char *keyList[5];
        char *typeList[5];
        char *fieldList[5];
        int listIndex = 3;
        keyList[0] = "\"Apr\"";
        typeList[0] = "notin";
        fieldList[0] = "DATE\n";
	keyList[1] = "\"Farming Fish\"";
        typeList[1] = "in";
        fieldList[1] = "BRAND_NAME\n";
	keyList[2] = "\"Soylent\"";
        typeList[2] = "in";
        fieldList[2] = "COMPANY\n";
        newRecords = filterRecords(newRecords, keyList, typeList, fieldList, listIndex);

        vector* expected = malloc(sizeof(vector));
        vector_init(expected);
        struct record* r1 = generateTestRecord("Wed, 26 Apr 2017 17:00:00 -0400","Farming Fish","Organic Basil Pesto","May be contaminated with Listeria monocytogenes.","Local Bounty","https://www.fda.gov/Safety/Recalls/ucm555399.htm","c");
        struct record* r2 = generateTestRecord("Mon, 24 Apr 2017 08:07:00 -0400","Soylent","1.8 Powder Bounty","Undeclared Milk","Soylent","https://www.fda.gov/Safety/Recalls/ucm554489.htm","g");
        struct record* r3 = generateTestRecord("a","a","a","a","a","a","a");
	VEC_ADD(*expected, r1);
        VEC_ADD(*expected, r2);
	VEC_ADD(*expected, r3);

        CU_ASSERT_EQUAL(compareVectors(newRecords,expected),1);
	freeRecords(&records);
	freeRecords(expected);
        VEC_FREE(records);
	VEC_FREE(*expected);
        free(expected);
}

void ifInputValidTest(char* command, int expected){
	char validationInput[1024] = {0};
	CU_ASSERT_EQUAL(expected,ifInputValid(strcpy(validationInput,command)));}


void test_01(void) { openFileTest("testfiles/inputfile01.xml", 0);}
void test_02(void) { openFileTest("testfiles/turtles", -1);}
void test_03(void) { openFileTest("testfiles/inputfile02.xml", 0);}
void test_04(void) { openFileTest("",-1);}

void test_05(void) { numberOfRecordsTest( "testfiles/RecallsDataSet2015-2017.xml",1120); }

void vectortest01(void) {vectorinitTest(true);}//tests initial size of vector set to zero
void vectortest02(void) {vectorSizeTest(1);}
void vectortest03(void) {vectorSizeTest(5);}
void vectortest04(void) {vectorSizeTest(10);}
void vectortest05(void) {vectorSizeTest(25);}
void vectortest06(void) {vectorSizeTest(1000);}

void vectortest07(void) {vectorResizeTest(5,8);}
void vectortest08(void) {vectorResizeTest(9,16);}
void vectortest09(void) {vectorResizeTest(21,32);}
void vectortest10(void) {vectorResizeTest(39,64);}
void vectortest11(void) {vectorResizeTest(1025,2048);}

void vectortest12(void) {vectorGetTest(0,"Matt");}
void vectortest13(void) {vectorGetTest(1,"Torri");}
void vectortest14(void) {vectorGetTest(2,"John");}
void vectortest15(void) {vectorGetTest(3,"Mark");}

void vectortest16(void) {vectorReplaceTest(1,"Bob");}

void vectortest17(void) {vectorOutofBoundsTest(NULL);}

void recordtest01(void) {recordinitTest(true);}

void parsetest01(void) {stripTest("\t<PRODUCT_DESCRIPTION>Bluth Frozen Bananas</PRODUCT_DESCRIPTION>","Bluth Frozen Bananas");}
void parsetest02(void) {stripTest("\t<DATE>5/4/2017</DATE>","5/4/2017");}
void parsetest03(void) {stripTest("\t<REASON>Hello!</REASON>","Hello!");}
void parsetest04(void) {stripTest("\t<PRODUCT_DESCRIPTION>Bluth Frozen Bananas</PRODUCT_DESCRIPTION>\n","Bluth Frozen Bananas");}
void parsetest05(void) {stripTest("\t<DATE>5/4/2017</DATE>\n","5/4/2017");}
void parsetest06(void) {stripTest("\t<REASON>Hello!</REASON>\n","Hello!");}
void parsetest07(void) {stripTest("\t<PRODUCT_DESCRIPTION>Bluth Frozen Bananas</PRODUCT_DESCRIPTION>\r\n","Bluth Frozen Bananas");}
void parsetest08(void) {stripTest("\t<DATE>5/4/2017</DATE>\r\n","5/4/2017");}
void parsetest09(void) {stripTest("\t<REASON>Hello!</REASON>\r\n","Hello!");}
void parsetest10(void) {stripTest("This is not a valid line.\n",NULL);}
void parsetest11(void) {stripTest("\t<BRAND_NAME>Nuka Cola</BRAND_NAME>","Nuka Cola");}
void parsetest12(void) {stripTest("\t<PHOTOS_LINK>http://www.google.com</PHOTOS_LINK>","http://www.google.com");}

void parsetest13(void) {stripInnerTest("![CDATA[Cold pressed apple juice]]","Cold pressed apple juice");}

void parsetest14(void) {fullStripTest("\t<PRODUCT_DESCRIPTION><![CDATA[Cold pressed apple juice]]></PRODUCT_DESCRIPTION>\r\n","Cold pressed apple juice");}
//void parsetest15(void) {}
void f2rtest01(void) {fieldTest(DATE,"test string");}
void f2rtest02(void) {fieldTest(BRAND_NAME,"test string");}
void f2rtest03(void) {fieldTest(REASON,"test string");}
void f2rtest04(void) {fieldTest(PRODUCT_DESCRIPTION,"test string");}

void infiltertest01(void) {inFilterTest1("testfiles/testInput3.xml");}
void infiltertest02(void) {inFilterTest2("testfiles/testInput3.xml");}
void notinfiltertest01(void) {notInFilterTest1("testfiles/testInput3.xml");}
void notinfiltertest02(void) {notInFilterTest2("testfiles/testInput3.xml");}
void allfiltertest01(void) {allFilterTest1("testfiles/testInput3.xml");}
void allfiltertest02(void) {allFilterTest2("testfiles/testInput3.xml");}
void anyfiltertest01(void) {anyFilterTest1("testfiles/testInput3.xml");}
void anyfiltertest02(void) {anyFilterTest2("testfiles/testInput3.xml");}
void orfiltertest01(void) {orFilterTest1("testfiles/testInput3.xml");}

void savetest01(void) {saveTest("testfiles/file1.txt",true);}
void savetest02(void) {saveTest("testfiles/file2.txt",false);}

void overwritetest01(void) {overwriteTest("testfiles/file1.txt",true);}
void overwritetest02(void) {overwriteTest("testfiles/file2.txt",true);}

void test_L00(void) { ifInputValidTest("\n",0);}
void test_L01(void) { ifInputValidTest("reset\n",1);}
void test_L02(void) { ifInputValidTest("re5et\n",0);}
void test_L03(void) { ifInputValidTest(" reset\n",0);}
void test_L04(void) { ifInputValidTest("re set\n",0);}
void test_L05(void) { ifInputValidTest("reset \n",0);}
void test_L06(void) { ifInputValidTest("quit\n",1);}
void test_L07(void) { ifInputValidTest("9uit\n",0);}
void test_L08(void) { ifInputValidTest(" quit\n",0);}
void test_L09(void) { ifInputValidTest("qu it\n",0);}
void test_L10(void) { ifInputValidTest("quit \n",0);}
void test_L11(void) { ifInputValidTest("count\n",1);}
void test_L12(void) { ifInputValidTest("c0unt\n",0);}
void test_L13(void) { ifInputValidTest(" count\n",0);}
void test_L14(void) { ifInputValidTest("co unt\n",0);}
void test_L15(void) { ifInputValidTest("count \n",0);}
void test_L16(void) { ifInputValidTest("print\n",1);}
void test_L17(void) { ifInputValidTest("prin7\n",0);}
void test_L18(void) { ifInputValidTest(" print\n",0);}
void test_L19(void) { ifInputValidTest("pr int\n",0);}
void test_L20(void) { ifInputValidTest("print\n ",0);}
void test_L21(void) { ifInputValidTest("help\n",1);}
void test_L22(void) { ifInputValidTest("h3lp\n",0);}
void test_L23(void) { ifInputValidTest(" help\n",0);}
void test_L24(void) { ifInputValidTest("he lp\n",0);}
void test_L25(void) { ifInputValidTest("help \n",0);}
void test_L26(void) { ifInputValidTest("save \"this.xml\"\n",1);}
//void test_L27(void) { ifInputValidTest("save this.xml\"\n",0);}
//void test_L28(void) { ifInputValidTest("save \"this.xml\n",0);}
void test_L29(void) { ifInputValidTest("save this.xml\n",1);}
void test_L30(void) { ifInputValidTest("save\n",0);}
void test_L31(void) { ifInputValidTest("overwrite \"this.xml\"\n",1);}
//void test_L32(void) { ifInputValidTest("overwrite this.xml\"\n",0);}
//void test_L33(void) { ifInputValidTest("overwrite \"this.xml\n",0);}
void test_L34(void) { ifInputValidTest("overwrite this.xml\n",1);}
void test_L35(void) { ifInputValidTest("overwrite\n",0);}
void test_L36(void) { ifInputValidTest("filter\n",0);}
void test_L37(void) { ifInputValidTest("filter \"string\"\n",0);}
void test_L38(void) { ifInputValidTest("filter string\"\n",0);}
void test_L39(void) { ifInputValidTest("filter \"string\n",0);}
void test_L40(void) { ifInputValidTest("filter string\n",0);}
void test_L41(void) { ifInputValidTest("filter \"string\" in\n",0);}
void test_L42(void) { ifInputValidTest("filter string\" in\n",0);}
void test_L43(void) { ifInputValidTest("filter \"string in\n",0);}
void test_L44(void) { ifInputValidTest("filter string in\n",0);}
void test_L45(void) { ifInputValidTest("filter \"string\" on\n",0);}
void test_L46(void) { ifInputValidTest("filter string\" on\n",0);}
void test_L47(void) { ifInputValidTest("filter \"string on\n",0);}
void test_L48(void) { ifInputValidTest("filter string on\n",0);}
void test_L49(void) { ifInputValidTest("filter \"string\" notin\n",0);}
void test_L50(void) { ifInputValidTest("filter string\" notin\n",0);}
void test_L51(void) { ifInputValidTest("filter \"string notin\n",0);}
void test_L52(void) { ifInputValidTest("filter string notin\n",0);}
void test_L53(void) { ifInputValidTest("filter \"string\" in SOMETHING\n",1);}
void test_L54(void) { ifInputValidTest("filter string\" in SOMETHING\n",0);}
void test_L55(void) { ifInputValidTest("filter \"string in SOMETHING\n",0);}
void test_L56(void) { ifInputValidTest("filter string in SOMETHING\n",0);}
void test_L57(void) { ifInputValidTest("filter \"string\" on SOMETHING\n",0);}
void test_L58(void) { ifInputValidTest("filter string\" on SOMETHING\n",0);}
void test_L59(void) { ifInputValidTest("filter \"string on SOMETHING\n",0);}
void test_L60(void) { ifInputValidTest("filter string on SOMETHING\n",0);}
void test_L61(void) { ifInputValidTest("filter \"string\" notin SOMETHING\n",1);}
void test_L62(void) { ifInputValidTest("filter string\" notin SOMETHING\n",0);}
void test_L63(void) { ifInputValidTest("filter \"string notin SOMETHING\n",0);}
void test_L64(void) { ifInputValidTest("filter string notin SOMETHING\n",0);}
void test_L65(void) { ifInputValidTest("filter \"string\" in SOMETHING AND\n",0);}
void test_L66(void) { ifInputValidTest("filter \"string\" in SOMETHING OR\n",0);}
void test_L67(void) { ifInputValidTest("filter \"string\" in SOMETHING AND filter\n",0);}
void test_L68(void) { ifInputValidTest("filter \"string\" in SOMETHING OR filter\n",0);}
void test_L69(void) { ifInputValidTest("filter \"string\" in SOMETHING AND filter \"string\"\n",0);}
void test_L70(void) { ifInputValidTest("filter \"string\" in SOMETHING AND filter string\"\n",0);}
void test_L71(void) { ifInputValidTest("filter \"string\" in SOMETHING AND filter \"string\n",0);}
void test_L72(void) { ifInputValidTest("filter \"string\" in SOMETHING AND filter string\n",0);}
void test_L73(void) { ifInputValidTest("filter \"string\" in SOMETHING OR filter \"string\"\n",0);}
void test_L74(void) { ifInputValidTest("filter \"string\" in SOMETHING OR filter string\"\n",0);}
void test_L75(void) { ifInputValidTest("filter \"string\" in SOMETHING OR filter \"string\n",0);}
void test_L76(void) { ifInputValidTest("filter \"string\" in SOMETHING OR filter string\n",0);}
void test_L77(void) { ifInputValidTest("filter \"string\" in SOMETHING AND filter \"string\" in\n",0);}
void test_L78(void) { ifInputValidTest("filter \"string\" in SOMETHING AND filter \"string\" notin\n",0);}
void test_L79(void) { ifInputValidTest("filter \"string\" in SOMETHING OR filter \"string\" in\n",0);}
void test_L80(void) { ifInputValidTest("filter \"string\" in SOMETHING OR filter \"string\" notin\n",0);}
void test_L81(void) { ifInputValidTest("filter \"string\" in SOMETHING AND filter \"string\" in SOMETHING\n",0);}
void test_L82(void) { ifInputValidTest("filter \"string\" in SOMETHING AND filter \"string\" notin SOMETHING\n",0);}
void test_L83(void) { ifInputValidTest("filter \"string\" in SOMETHING OR filter \"string\" in SOMETHING\n",1);}
void test_L84(void) { ifInputValidTest("filter \"string\" in SOMETHING OR filter \"string\" notin SOMETHING\n",1);}
void test_L85(void) { ifInputValidTest("save \"this.xml\" extra\n",0);}
void test_L86(void) { ifInputValidTest("save this.xml\" extra\n",0);}
void test_L87(void) { ifInputValidTest("save \"this.xml extra\n",0);}
void test_L88(void) { ifInputValidTest("save this.xml extra\n",0);}
void test_L89(void) { ifInputValidTest("save extra\n",1);}
void test_L90(void) { ifInputValidTest("overwrite \"this.xml\" extra\n",0);}
void test_L91(void) { ifInputValidTest("overwrite this.xml\" extra\n",0);}
void test_L92(void) { ifInputValidTest("overwrite \"this.xml extra\n",0);}
void test_L93(void) { ifInputValidTest("overwrite this.xml extra\n",0);}
void test_L94(void) { ifInputValidTest("overwrite extra\n",1);}
void test_L95(void) { ifInputValidTest("filter \"string\" oo SOMETHING AND filter \"string\" in SOMETHING\n",0);}
void test_L96(void) { ifInputValidTest("filter \"string\" oo SOMETHING AND filter \"string\" notin SOMETHING\n",0);}
void test_L97(void) { ifInputValidTest("filter \"string\" oo SOMETHING OR filter \"string\" in SOMETHING\n",0);}
void test_L98(void) { ifInputValidTest("filter \"string\" oo SOMETHING OR filter \"string\" notin SOMETHING\n",0);}
void test_L99(void) { ifInputValidTest("filter \"string\" in SOMETHING AND filter \"string\" oo SOMETHING\n",0);}
void test_LA0(void) { ifInputValidTest("filter \"string\" in SOMETHING AND filter \"string\" no SOMETHING\n",0);}
void test_LA1(void) { ifInputValidTest("filter \"string\" in SOMETHING OR filter \"string\" on SOMETHING\n",0);}
void test_LA2(void) { ifInputValidTest("filter \"string\" in SOMETHING OR filter \"string\" non SOMETHING\n",0);}
void test_LA3(void) { ifInputValidTest("save \"this.xml\" extra extra\n",0);}
void test_LA4(void) { ifInputValidTest("save this.xml\" extra extra\n",0);}
void test_LA5(void) { ifInputValidTest("save \"this.xml extra extra\n",0);}
void test_LA6(void) { ifInputValidTest("save this.xml extra extra\n",0);}
void test_LA7(void) { ifInputValidTest("save extra extra\n",0);}
void test_LA8(void) { ifInputValidTest("overwrite \"this.xml\" extra extra\n",0);}
void test_LA9(void) { ifInputValidTest("overwrite this.xml\" extra extra\n",0);}
void test_LB0(void) { ifInputValidTest("overwrite \"this.xml extra extra\n",0);}
void test_LB1(void) { ifInputValidTest("overwrite this.xml extra extra\n",0);}
void test_LB2(void) { ifInputValidTest("overwrite extra extra\n",0);}


int init_suite1(void) { return 0; }    /* The suite initialization function. */
int clean_suite1(void) { return 0; }   /* The suite cleanup function. */

/* The main() function for setting up and running the tests.
 *  * Returns a CUE_SUCCESS on successful running, another
 *   * CUnit error code on failure.
 *    */
int main()
{
   CU_pSuite pSuite = NULL;

   /* initialize the CUnit test registry */
   if (CUE_SUCCESS != CU_initialize_registry()) { return CU_get_error(); }

   /* add a suite to the registry */
   pSuite = CU_add_suite("Suite_1", init_suite1, clean_suite1);
   if (NULL == pSuite) {
      CU_cleanup_registry();
      return CU_get_error();
   }

   /* add the tests to the suite */

   if (
          (NULL == CU_add_test(pSuite, "inputfile01", test_01))
       || (NULL == CU_add_test(pSuite, "bad filename", test_02))
|| (NULL == CU_add_test(pSuite, "inputfile02", test_03))
|| (NULL == CU_add_test(pSuite, "empty filename", test_04))
|| (NULL == CU_add_test(pSuite, "count records", test_05))

|| (NULL == CU_add_test(pSuite, "vector init", vectortest01))

|| (NULL == CU_add_test(pSuite, "vector size 1", vectortest02))
|| (NULL == CU_add_test(pSuite, "vector size 5", vectortest03))
|| (NULL == CU_add_test(pSuite, "vector size 10", vectortest04))
|| (NULL == CU_add_test(pSuite, "vector size 25", vectortest05))
|| (NULL == CU_add_test(pSuite, "vector size 1000", vectortest06))

|| (NULL == CU_add_test(pSuite, "vector resize 5", vectortest07))
|| (NULL == CU_add_test(pSuite, "vector resize 9", vectortest08))
|| (NULL == CU_add_test(pSuite, "vector resize 21", vectortest09))
|| (NULL == CU_add_test(pSuite, "vector resize 39", vectortest10))
|| (NULL == CU_add_test(pSuite, "vector resize 1025", vectortest11))

|| (NULL == CU_add_test(pSuite, "vector get", vectortest12))
|| (NULL == CU_add_test(pSuite, "vector get", vectortest13))
|| (NULL == CU_add_test(pSuite, "vector get", vectortest14))
|| (NULL == CU_add_test(pSuite, "vector get", vectortest15))

|| (NULL == CU_add_test(pSuite, "vector replace", vectortest16))

|| (NULL == CU_add_test(pSuite, "vector get out of bounds", vectortest17))

|| (NULL == CU_add_test(pSuite, "record init test", recordtest01))

|| (NULL == CU_add_test(pSuite, "strip test 1", parsetest01))
|| (NULL == CU_add_test(pSuite, "strip test 2", parsetest02))
|| (NULL == CU_add_test(pSuite, "strip test 3", parsetest03))
|| (NULL == CU_add_test(pSuite, "strip test 4", parsetest04))
|| (NULL == CU_add_test(pSuite, "strip test 5", parsetest05))
|| (NULL == CU_add_test(pSuite, "strip test 6", parsetest06))
|| (NULL == CU_add_test(pSuite, "strip test 7", parsetest07))
|| (NULL == CU_add_test(pSuite, "strip test 8", parsetest08))
|| (NULL == CU_add_test(pSuite, "strip test 9", parsetest09))
|| (NULL == CU_add_test(pSuite, "strip test 10", parsetest10))
|| (NULL == CU_add_test(pSuite, "strip test 11", parsetest11))
|| (NULL == CU_add_test(pSuite, "strip test 12", parsetest12))      

|| (NULL == CU_add_test(pSuite, "strip test 13", parsetest13))

|| (NULL == CU_add_test(pSuite, "strip test 14", parsetest14))

|| (NULL == CU_add_test(pSuite, "f2rtest01", f2rtest01))
|| (NULL == CU_add_test(pSuite, "f2rtest02", f2rtest02))

|| (NULL == CU_add_test(pSuite, "infiltertest01", infiltertest01))
|| (NULL == CU_add_test(pSuite, "infiltertest02", infiltertest02))
|| (NULL == CU_add_test(pSuite, "notinfiltertest01", notinfiltertest01))
|| (NULL == CU_add_test(pSuite, "notinfiltertest02", notinfiltertest02))
|| (NULL == CU_add_test(pSuite, "allfiltertest01", allfiltertest01))
|| (NULL == CU_add_test(pSuite, "allfiltertest02", allfiltertest02))
|| (NULL == CU_add_test(pSuite, "anyfiltertest01", anyfiltertest01))
|| (NULL == CU_add_test(pSuite, "anyfiltertest02", anyfiltertest02))
|| (NULL == CU_add_test(pSuite, "orfiltertest01", orfiltertest01))
|| (NULL == CU_add_test(pSuite, "save01", savetest01))
|| (NULL == CU_add_test(pSuite, "save02", savetest02))
|| (NULL == CU_add_test(pSuite, "overwrite01", overwritetest01))
|| (NULL == CU_add_test(pSuite, "overwrite02", overwritetest02))

|| (NULL == CU_add_test(pSuite, "L00", test_L00))
|| (NULL == CU_add_test(pSuite, "L01", test_L01))
|| (NULL == CU_add_test(pSuite, "L02", test_L02))
|| (NULL == CU_add_test(pSuite, "L03", test_L03))
|| (NULL == CU_add_test(pSuite, "L04", test_L04))
|| (NULL == CU_add_test(pSuite, "L05", test_L05))
|| (NULL == CU_add_test(pSuite, "L06", test_L06))
|| (NULL == CU_add_test(pSuite, "L07", test_L07))
|| (NULL == CU_add_test(pSuite, "L08", test_L08))
|| (NULL == CU_add_test(pSuite, "L09", test_L09))
|| (NULL == CU_add_test(pSuite, "L10", test_L10))
|| (NULL == CU_add_test(pSuite, "L11", test_L11))
|| (NULL == CU_add_test(pSuite, "L12", test_L12))
|| (NULL == CU_add_test(pSuite, "L13", test_L13))
|| (NULL == CU_add_test(pSuite, "L14", test_L14))
|| (NULL == CU_add_test(pSuite, "L15", test_L15))
|| (NULL == CU_add_test(pSuite, "L16", test_L16))
|| (NULL == CU_add_test(pSuite, "L17", test_L17))
|| (NULL == CU_add_test(pSuite, "L18", test_L18))
|| (NULL == CU_add_test(pSuite, "L19", test_L19))
|| (NULL == CU_add_test(pSuite, "L20", test_L20))
|| (NULL == CU_add_test(pSuite, "L21", test_L21))
|| (NULL == CU_add_test(pSuite, "L22", test_L22))
|| (NULL == CU_add_test(pSuite, "L23", test_L23))
|| (NULL == CU_add_test(pSuite, "L24", test_L24))
|| (NULL == CU_add_test(pSuite, "L25", test_L25))
|| (NULL == CU_add_test(pSuite, "L26", test_L26))
//|| (NULL == CU_add_test(pSuite, "L27", test_L27))
//|| (NULL == CU_add_test(pSuite, "L28", test_L28))
|| (NULL == CU_add_test(pSuite, "L29", test_L29))
|| (NULL == CU_add_test(pSuite, "L30", test_L30))
|| (NULL == CU_add_test(pSuite, "L31", test_L31))
//|| (NULL == CU_add_test(pSuite, "L32", test_L32))
//|| (NULL == CU_add_test(pSuite, "L33", test_L33))
|| (NULL == CU_add_test(pSuite, "L34", test_L34))
|| (NULL == CU_add_test(pSuite, "L35", test_L35))
|| (NULL == CU_add_test(pSuite, "L36", test_L36))
|| (NULL == CU_add_test(pSuite, "L37", test_L37))
|| (NULL == CU_add_test(pSuite, "L38", test_L38))
|| (NULL == CU_add_test(pSuite, "L39", test_L39))
|| (NULL == CU_add_test(pSuite, "L40", test_L40))
|| (NULL == CU_add_test(pSuite, "L41", test_L41))
|| (NULL == CU_add_test(pSuite, "L42", test_L42))
|| (NULL == CU_add_test(pSuite, "L43", test_L43))
|| (NULL == CU_add_test(pSuite, "L44", test_L44))
|| (NULL == CU_add_test(pSuite, "L45", test_L45))
|| (NULL == CU_add_test(pSuite, "L46", test_L46))
|| (NULL == CU_add_test(pSuite, "L47", test_L47))
|| (NULL == CU_add_test(pSuite, "L48", test_L48))
|| (NULL == CU_add_test(pSuite, "L49", test_L49))
|| (NULL == CU_add_test(pSuite, "L50", test_L50))
|| (NULL == CU_add_test(pSuite, "L51", test_L51))
|| (NULL == CU_add_test(pSuite, "L52", test_L52))
|| (NULL == CU_add_test(pSuite, "L53", test_L53))
|| (NULL == CU_add_test(pSuite, "L54", test_L54))
|| (NULL == CU_add_test(pSuite, "L55", test_L55))
|| (NULL == CU_add_test(pSuite, "L56", test_L56))
|| (NULL == CU_add_test(pSuite, "L57", test_L57))
|| (NULL == CU_add_test(pSuite, "L58", test_L58))
|| (NULL == CU_add_test(pSuite, "L59", test_L59))
|| (NULL == CU_add_test(pSuite, "L60", test_L60))
|| (NULL == CU_add_test(pSuite, "L61", test_L61))
|| (NULL == CU_add_test(pSuite, "L62", test_L62))
|| (NULL == CU_add_test(pSuite, "L63", test_L63))
|| (NULL == CU_add_test(pSuite, "L64", test_L64))
|| (NULL == CU_add_test(pSuite, "L65", test_L65))
|| (NULL == CU_add_test(pSuite, "L66", test_L66))
|| (NULL == CU_add_test(pSuite, "L67", test_L67))
|| (NULL == CU_add_test(pSuite, "L68", test_L68))
|| (NULL == CU_add_test(pSuite, "L69", test_L69))
|| (NULL == CU_add_test(pSuite, "L70", test_L70))
|| (NULL == CU_add_test(pSuite, "L71", test_L71))
|| (NULL == CU_add_test(pSuite, "L72", test_L72))
|| (NULL == CU_add_test(pSuite, "L73", test_L73))
|| (NULL == CU_add_test(pSuite, "L74", test_L74))
|| (NULL == CU_add_test(pSuite, "L75", test_L75))
|| (NULL == CU_add_test(pSuite, "L76", test_L76))
|| (NULL == CU_add_test(pSuite, "L77", test_L77))
|| (NULL == CU_add_test(pSuite, "L78", test_L78))
|| (NULL == CU_add_test(pSuite, "L79", test_L79))
|| (NULL == CU_add_test(pSuite, "L80", test_L80))
|| (NULL == CU_add_test(pSuite, "L81", test_L81))
|| (NULL == CU_add_test(pSuite, "L82", test_L82))
|| (NULL == CU_add_test(pSuite, "L83", test_L83))
|| (NULL == CU_add_test(pSuite, "L84", test_L84))
|| (NULL == CU_add_test(pSuite, "L85", test_L85))
|| (NULL == CU_add_test(pSuite, "L86", test_L86))
|| (NULL == CU_add_test(pSuite, "L87", test_L87))
|| (NULL == CU_add_test(pSuite, "L88", test_L88))
|| (NULL == CU_add_test(pSuite, "L89", test_L89))
|| (NULL == CU_add_test(pSuite, "L90", test_L90))
|| (NULL == CU_add_test(pSuite, "L91", test_L91))
|| (NULL == CU_add_test(pSuite, "L92", test_L92))
|| (NULL == CU_add_test(pSuite, "L93", test_L93))
|| (NULL == CU_add_test(pSuite, "L94", test_L94))
|| (NULL == CU_add_test(pSuite, "L95", test_L95))
|| (NULL == CU_add_test(pSuite, "L96", test_L96))
|| (NULL == CU_add_test(pSuite, "L97", test_L97))
|| (NULL == CU_add_test(pSuite, "L98", test_L98))
|| (NULL == CU_add_test(pSuite, "L99", test_L99))
|| (NULL == CU_add_test(pSuite, "LA0", test_LA0))
|| (NULL == CU_add_test(pSuite, "LA1", test_LA1))
|| (NULL == CU_add_test(pSuite, "LA2", test_LA2))
|| (NULL == CU_add_test(pSuite, "LA3", test_LA3))
|| (NULL == CU_add_test(pSuite, "LA4", test_LA4))
|| (NULL == CU_add_test(pSuite, "LA5", test_LA5))
|| (NULL == CU_add_test(pSuite, "LA6", test_LA6))
|| (NULL == CU_add_test(pSuite, "LA7", test_LA7))
|| (NULL == CU_add_test(pSuite, "LA8", test_LA8))
|| (NULL == CU_add_test(pSuite, "LA9", test_LA9))
|| (NULL == CU_add_test(pSuite, "LB0", test_LB0))
|| (NULL == CU_add_test(pSuite, "LB1", test_LB1))
|| (NULL == CU_add_test(pSuite, "LB2", test_LB2))
)

   {
      CU_cleanup_registry();
      return CU_get_error();
   }

   /* Run all tests using the CUnit Basic interface */
   CU_basic_set_mode(CU_BRM_VERBOSE);
   CU_basic_run_tests();
   CU_cleanup_registry();
   return CU_get_error();
}

