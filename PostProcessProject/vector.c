#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#ifndef VECTOR_H
#include "vector.h"
#define VECTOR_H
#endif

#include "post.h"

#define INITIAL_CAPACITY 4;

void vector_init(vector* vec){
	vec->capacity = INITIAL_CAPACITY;
	vec->next = 0;
	vec->items = malloc(sizeof(void*) * vec->capacity);
}

void vector_add(vector* vec, void* item){
	if(vec->next == vec->capacity){//resize needed
		vec->items = realloc(vec->items, sizeof(void*) * vec->capacity * 2);
		vec->capacity = vec->capacity * 2;
	}
	vec->items[vec->next] = item;
	vec->next++;
}
void vector_replace(vector*vec, int index, void* item){
	if(index<vec->next){
		vec->items[index] = item;
	}
}
void* vector_get(vector* vec, int index){
	if(index<vec->next){
		return vec->items[index];
	}
	else{
		return NULL;
	}
}
int vector_size(vector* vec){
	return vec->next;
}
void vector_free(vector* vec){
	free(vec->items);
}

