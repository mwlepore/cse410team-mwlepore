#ifndef VECTOR_H
#define VECTOR_H

#define VEC_INIT(vec) vector vec; vector_init(&vec)
#define VEC_ADD(vec,item) vector_add(&vec, (void*)item)
#define VEC_REPLACE(vec,index,item) vector_replace(&vec, index, (void*)item)
#define VEC_GET(vec, index, type) (type)vector_get(&vec, index)
#define VEC_SIZE(vec) vector_size(&vec)
#define VEC_FREE(vec) vector_free(&vec)
#define INITIAL_CAPACITY 4;

typedef struct{
	void** items;
	int capacity;
	int next;
}vector;

void vector_init(vector*);

void vector_add(vector*, void*);

void vector_replace(vector*, int, void*);

void* vector_get(vector*, int);

int vector_size(vector*);

void vector_free(vector*);
#endif
