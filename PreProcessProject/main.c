#include <stdio.h>
#include <stdbool.h>
#include <stdlib.h>
#include <string.h>

#define STACKSIZE 32

/*Pads string with whitespace around left of right parenthesis to make further processing easier.*/
char* addSpaces(char* input){
  char* output = malloc(2*strlen(input)*sizeof(char));
  for(int i=0;i<strlen(input)*2;i++){
	output[i] = '\0';
  }
int i = 0;
int j = 0;

for(int i=0;input[i]!=0;i++){
  if(input[i]=='('){
    output[j++] = ' ';
    output[j++] = '(';
    output[j++] = ' ';
  }
  else if(input[i]==')'){
    output[j++] = ' ';
    output[j++] = ')';
    output[j++] = ' ';
  }
  else{
    output[j] = input[i];
    j++;
  }
}
  output[j] = '\0';

  return output;  
}


struct StackValue{
  int payload;
  bool isOperator;
  char operator;
};

/*Stack used in computation.*/
struct Stack{
  int top;
  int arr[STACKSIZE];
};

/*Initializes stack's top value to NULL.*/
void initStack(struct Stack* s){
  s->top = 0;
}

/*Pushes an integer to computation stack.*/
void push(struct Stack* s, int val){
  s->arr[s->top] = val;
  s->top++;
}

/*Removes and returns the top item on the computation stack.*/
int pop(struct Stack* s){
  if(s->top!=0){
    return s->arr[--s->top];
  }
  else{printf("Trying to pop empty stack!  Fool!\n");}
}

/*Returns the top item on the computation stack.*/
int peek(struct Stack* s){
  if(s->top!=0){
    return s->arr[(s->top)-1];
  }
  else{printf("Trying to peek empty stack!  Fool!\n");}
}


/*Given an string representing a valid postfix expression, calulates the numerical value of the expression.*/
char* calculate(char* postfix){
  struct Stack s;
  initStack(&s);
  char* result = malloc(sizeof(char)*256);
  char *token;
  const char delim[2] = " "; 
   token = strtok(postfix, delim);
   while(token != NULL) 
   {
      int t = atoi(token);
    if(strcmp(token,"0")==0){push(&s,0);}
	else if(t!=0){push(&s,t);}
    else{
		  
		if(strcmp(token,"+")==0){
			int x = pop(&s);
			int y = pop(&s);
			push(&s,y+x);
		}
		else if(strcmp(token,"-")==0){
			int x = pop(&s);
			int y = pop(&s);
			push(&s,y-x);
		}
		else if(strcmp(token,"*")==0){
			int x = pop(&s);
			int y = pop(&s);
			push(&s,y*x);
		}
		else if(strcmp(token,"/")==0){
			int x = pop(&s);
			int y = pop(&s);
			if(x==0){result = "Error!\0";return result;}
			push(&s,y/x);	
		}
    }
    token = strtok(NULL, delim);
   }
  int intresult = pop(&s);
  sprintf(result, "%d\0",intresult);
  return result;
}

struct Something{
	bool isOperator;
	int value;
	char operator;	
};

char *post(char *ptr_input){	//reference source: https://www.tutorialspoint.com/c_standard_library/c_function_strtok.htm
	int operator_index=0;
	int postfix_index=0;
	struct Something operator_array_of_something[128];
	struct Something postfix_array_of_something[128];
	const char s[2]=" ";
	char *token;
   	token=strtok(ptr_input,s);
	while(token!=NULL){
		char c;
		struct Something something;
		if(atoi(token)==0&&token[strlen(token)-1]!='0'&&strlen(token)==1){
			something.isOperator=true;
			something.operator=token[0];
		}
		else{
			something.isOperator=false;
			something.value=atoi(token);
		}
		if(something.isOperator){	//if it is operator
			if(operator_index==0)
				c='0';
			else if(something.operator=='(')
				c='0';
			else if(something.operator=='+'||something.operator=='-'){
				if(operator_array_of_something[operator_index-1].operator=='(')
					c='0';
				else if(operator_array_of_something[operator_index-1].operator=='+'||operator_array_of_something[operator_index-1].operator=='-')
					c='1';
				else if(operator_array_of_something[operator_index-1].operator=='*'||operator_array_of_something[operator_index-1].operator=='/')
					c='1';
			}
			else if(something.operator=='*'||something.operator=='/'){
				if(operator_array_of_something[operator_index-1].operator=='(')
					c='0';
				else if(operator_array_of_something[operator_index-1].operator=='+'||operator_array_of_something[operator_index-1].operator=='-')
					c='0';
				else if(operator_array_of_something[operator_index-1].operator=='*'||operator_array_of_something[operator_index-1].operator=='/')
					c='1';
			}
			else if(something.operator==')')
				c='3';
		}
		else				//if it is value
			c='2';
		switch(c) {
			case '0' :	//push into operator array
				operator_array_of_something[operator_index]=something;
				operator_index++;
				break;
			case '1' :	//pop operator array, push into postfix array
				postfix_array_of_something[postfix_index]=operator_array_of_something[operator_index-1];
				postfix_index++;
				operator_array_of_something[operator_index-1]=something;
				if((something.operator=='+'||something.operator=='-')&&(operator_array_of_something[operator_index-2].operator=='+'||operator_array_of_something[operator_index-2].operator=='-')){
					postfix_array_of_something[postfix_index]=operator_array_of_something[operator_index-2];
					postfix_index++;
					operator_array_of_something[operator_index-2]=something;
					operator_index--;
				}
				break;
			case '2' :	//push number into postfix array
				postfix_array_of_something[postfix_index]=something;
				postfix_index++;
				break;
			case '3' :	//operator '('
					while(operator_array_of_something[operator_index-1].operator!='('){
					postfix_array_of_something[postfix_index]=operator_array_of_something[operator_index-1];
					postfix_index++;
					operator_index--;
				}
				operator_index--;
				break;
			default :
				printf("error\n");
		}
		token = strtok(NULL, s);
	}
	while (operator_index!=0){	//take cares what is remain in the operator array
		postfix_array_of_something[postfix_index]=operator_array_of_something[operator_index-1];
		postfix_index++;
		operator_index--;
	}
	int n=0;
	char output[128]={0};
	for(int i=0;i<postfix_index;i++){
		if(postfix_array_of_something[i].isOperator){	// if is operator
			output[n]=postfix_array_of_something[i].operator;
			n++;
			output[n]=' ';
			n++;
		}
		else{						//if it is number
			char buffer[64];
			sprintf(buffer,"%d",postfix_array_of_something[i].value);
			for(int j=0;j<strlen(buffer);j++){
				output[n]=buffer[j];
				n++;
			}
			output[n]=' ';
			n++;	
		}
	}
	output[n-1]='\0';
	char *ptr_output=output;
	return ptr_output;
}
	
char *reverse(char *ptr_input){
	int c=0;
	while(ptr_input[c]!='\0')
		c++;	
	int n=0;
	static char output[128]={0};
	for(int i=c-1;i>=0;i--){
		if(ptr_input[i]==' '){
			for(int j=i+1;j<c;j++){
				if(ptr_input[j]=='(')
					output[n]=')';
				else if(ptr_input[j]==')')
					output[n]='(';
				else
					output[n]=ptr_input[j];
				n++;
			}
			output[n]=' ';
			n++;
			c=i;
		}
	}
	for(int i=0;i<c;i++){
		if(ptr_input[i]=='(')
			output[n]=')';
		else if(ptr_input[i]==')')
			output[n]='(';
		else
			output[n]=ptr_input[i];
		n++;
	}
	output[n]='\0';
	char *ptr_output=output;
	return ptr_output;
}
	
char *pre(char *ptr_input){
	char *ptr_reverse=reverse(ptr_input);
	char *ptr_postfix=post(ptr_reverse);
	char *ptr_output=reverse(ptr_postfix);
	return ptr_output;
}






int main(int argc, char **argv){
/*INPUT*/
  if(argc!=2){printf("Please provide a filename.\n");return 1;}
  char* inputfilename = malloc(sizeof(char)*(strlen(argv[1])+3));
  char* outputfilename = malloc(sizeof(char)*(strlen(argv[1])+3));
  bool errorFound = false;
  strcpy(inputfilename, argv[1]);
  strcat(inputfilename, ".IN");
  //printf("%s\n",inputfilename);
  strcpy(outputfilename, argv[1]);
  strcat(outputfilename, ".OUT");
  //printf("%s\n",outputfilename);

  char buffer[256] = {0};
  FILE *in;
  in = fopen(inputfilename, "r");
  if(in==NULL){printf("%s did not open successfully!  Does it exist?\n",inputfilename);free(inputfilename);free(outputfilename);return 1;}
  else{/*printf("File successfully opened.\n");*/}
  
  fgets(buffer, 255, in);
  
  if(fclose(in)==0){
    free(inputfilename);
    //printf("File successfully closed.\n");
  }  
/*END OF INPUT*/






/*PROCESSING*/
    buffer[strlen(buffer)-1] = '\0';//strips off newline

//    printf("Buffer: %s -- Length: %d\n",buffer,strlen(buffer));
    char* mod = addSpaces(buffer);
    //printf("Modified input string: %s -- Length: %d\n",mod,strlen(mod));

	char processinginput[256] = {0};
	strcpy(processinginput, mod);
	char processingoutput[256] = {0}; //This is the final string we will want to work with
	printf("%s\n",processinginput);
	char *token;
  	const char s[2] = " ";
	token = strtok(processinginput, s);
	int counter = 0;

	while( token != NULL ){
		if(token[0] == 'I' || token[0] == 'i' || token[0] == 'V' || token[0] == 'v' || token[0] == 'X' || token[0] == 'x' || token[0] == 'L' || token[0] == 'l' || token[0] == 'C' || token[0] == 'c' || token[0] == 'D' || token[0] == 'd' || token[0] == 'M' || token[0] == 'm'){
		
			int total = 0;
			int length = strlen(token)-1;

			if(token[length] == 'I' || token[length] == 'i'){
				total = total + 1;
			}
			else if(token[length] == 'V' || token[length] == 'v'){
				total = total + 5;
			}
			else if(token[length] == 'X' || token[length] == 'x'){
                                total = total + 10;
                        }
			else if(token[length] == 'L' || token[length] == 'l'){
                                total = total + 50;
                        }
			else if(token[length] == 'C' || token[length] == 'c'){
                                total = total + 100;
                        }
			else if(token[length] == 'D' || token[length] == 'd'){
                                total = total + 500;
                        }
			else if(token[length] == 'M' || token[length] == 'm'){
                                total = total + 1000;
                        }




			for(int i=length-1; i>=0; i--){
				int current;
				int previous;

				// Prevents duplicate Vs, Ls, or Ds
				if((token[i] == 'V' || token[i] == 'v') && (token[i+1] == 'V' || token[i+1] == 'v')){
					errorFound = true;
				}

				if((token[i] == 'L' || token[i] == 'l') && (token[i+1] == 'L' || token[i+1] == 'l')){
                                        errorFound = true;
                                }

				if((token[i] == 'D' || token[i] == 'd') && (token[i+1] == 'D' || token[i+1] == 'd')){
                                        errorFound = true;
                                }




				if(token[i] == 'I' || token[i] == 'i'){
                                	current = 1;
                                }
                                else if(token[i] == 'V' || token[i] == 'v'){
                                        current = 5;
                                }
                                else if(token[i] == 'X' || token[i] == 'x'){
                                        current = 10;
                                }
                                else if(token[i] == 'L' || token[i] == 'l'){
                                        current = 50;
                                }
                                else if(token[i] == 'C' || token[i] == 'c'){
                                        current = 100;
                                }
                                else if(token[i] == 'D' || token[i] == 'd'){
                                        current = 500;
                                }
                                else if(token[i] == 'M' || token[i] == 'm'){
                                        current = 1000;
                                }

				if(token[i+1] == 'I' || token[i+1] == 'i'){
                                        previous = 1;
                                }
                                else if(token[i+1] == 'V' || token[i+1] == 'v'){
                                        previous = 5;
                                }
                                else if(token[i+1] == 'X' || token[i+1] == 'x'){
                                        previous = 10;
                                }
                                else if(token[i+1] == 'L' || token[i+1] == 'l'){
                                        previous = 50;
                                }
                                else if(token[i+1] == 'C' || token[i+1] == 'c'){
                                        previous = 100;
                                }
                                else if(token[i+1] == 'D' || token[i+1] == 'd'){
                                        previous = 500;
                                }
                                else if(token[i+1] == 'M' || token[i+1] == 'm'){
                                        previous = 1000;
                                }



				if(current < previous){
					if(token[i] == 'I' || token[i] == 'i'){
                                                total = total - 1;
                                        }
                                        else if(token[i] == 'V' || token[i] == 'v'){
                                                total = total - 5;
                                        }
                                        else if(token[i] == 'X' || token[i] == 'x'){
                                                total = total - 10;
                                        }
                                        else if(token[i] == 'L' || token[i] == 'l'){
                                                total = total - 50;
                                        }
                                        else if(token[i] == 'C' || token[i] == 'c'){
                                                total = total - 100;
                                        }
                                        else if(token[i] == 'D' || token[i] == 'd'){
                                                total = total - 500;
                                        }
                                        else if(token[i] == 'M' || token[i] == 'm'){
                                                total = total - 1000;
                                        }
				}
				else{
					if(token[i] == 'I' || token[i] == 'i'){
                                		total = total + 1;
                        		}
                        		else if(token[i] == 'V' || token[i] == 'v'){
                                		total = total + 5;
                        		}
                        		else if(token[i] == 'X' || token[i] == 'x'){
                                		total = total + 10;
                        		}
                        		else if(token[i] == 'L' || token[i] == 'l'){
                                		total = total + 50;
                        		}
                        		else if(token[i] == 'C' || token[i] == 'c'){
                                		total = total + 100;
                        		}
                        		else if(token[i] == 'D' || token[i] == 'd'){
                                		total = total + 500;
                        		}
                        		else if(token[i] == 'M' || token[i] == 'm'){
                                		total = total + 1000;
                        		}
				}
			}
			/*printf("%s", "The decimal value of ");
			printf("%s", token);
			printf("%s", " is ");
			printf("%d", total);
			printf("%s", "\n");*/

			char totalBuffer[256] = {0};
			sprintf(totalBuffer, "%d", total);
			for(int z=0; z<strlen(totalBuffer); z++){
				processingoutput[counter] = totalBuffer[z];
				counter = counter + 1;
			}
			processingoutput[counter] = ' ';
			counter = counter + 1;
		}
		else{
                	for(int j=0; j<strlen(token); j++){
                        	processingoutput[counter] = token[j];
                        	counter = counter + 1;
                	}
                	processingoutput[counter] = ' ';
                	counter = counter + 1;
        	}
      		token = strtok(NULL, s);
   	}

	char copy[256] = {0};
	strcpy(copy, processingoutput);
	token = strtok(copy, s);

	if(strcmp(token,"+")==0){ // Makes sure input does not start with operator
		errorFound = true;
        }
        if(strcmp(token,"-")==0){
                errorFound = true;
        }
        if(strcmp(token,"*")==0){
                errorFound = true;
        }
        if(strcmp(token,"/")==0){
                errorFound = true;
        }
	

	int wasPreviousOperator = 0;
	int wasPreviousInt = 0;
	int numOpenParen = 0;
	int numCloseParen = 0;

	while(token != NULL){
		for(int i=0; i<strlen(token); i++){ // Makes sure input does not use any foreign characters
			if(token[i]!='0' && token[i]!='1' && token[i]!='2' && token[i]!='3' && token[i]!='4' && token[i]!='5' && token[i]!='6' && token[i]!='7' && token[i]!='8' && token[i]!='9' && token[i]!='(' && token[i]!=')' && token[i]!='+' && token[i]!='-' && token[i]!='*' && token[i]!='/'){
				/*printf("%s", "Error!\n");
				printf("%s", "Case 5\n");
				printf("%s", "Here is the new token: ");
				printf("%c", token[i]);
				printf("\n");
				return 0;*/
			}
		}

		// Makes sure no two operators are directly next to each other
		if(strcmp(token,"+")==0 || strcmp(token,"-")==0 || strcmp(token,"*")==0 || strcmp(token,"/")==0){
			if(wasPreviousOperator == 1){
				errorFound = true;
			}
			wasPreviousOperator = 1;
		}
		else{
			wasPreviousOperator = 0;
		}

		// Makes sure no two integers are directly next to each other
		if(strcmp(token,"+")!=0 && strcmp(token,"-")!=0 && strcmp(token,"*")!=0 && strcmp(token,"/")!=0 && strcmp(token,"(")!=0 && strcmp(token,")")!=0){
                        if(wasPreviousInt == 1){
                               errorFound = true;
                        }
                        wasPreviousInt = 1;
                }
                else{
                        wasPreviousInt = 0;
                }




		

		// Makes sure same number of opening and closing parens
		if(strcmp(token,"(")==0){
			numOpenParen = numOpenParen + 1;
		}

		if(strcmp(token,")")==0){
                        numCloseParen = numCloseParen + 1;
                }

 



   
      		token = strtok(NULL, s);
	}


	// Actually compares number of opening and closing parens
	if(numOpenParen != numCloseParen){
		errorFound = true;
	}
	//char *result;
	char *outpre;
	char *outpost;
	char *outresult;
	//char *result;
	if(!errorFound){
	    	char input[256] = {0};
        	strcpy(input, processingoutput);
	//	printf("Copied input string: %s -- Length: %d\n",input,strlen(input));
		char input_copy_1[128]={0};	//for prefix
		char input_copy_2[128]={0};	//for postfix	
		char input_copy_3[128]={0};
		int n=0;
		for(int i=0;i<strlen(input);i++){
			input_copy_1[i]=input[i];
			input_copy_2[i]=input[i];
			input_copy_3[i]=input[i];
			n++;
		}
		input_copy_1[n]='\0';
		input_copy_2[n]='\0';
		input_copy_3[n]='\0';
		char *ptr_input_copy_1=input_copy_1;
		char *ptr_input_copy_2=input_copy_2;
		char *ptr_input_copy_3=input_copy_3;	
			
		char *ptr_prefix=pre(ptr_input_copy_1);
		char *ptr_postfix=post(input_copy_2);
		
		outpre = malloc(sizeof(char)*(strlen(ptr_prefix)+20));
		outpost = malloc(sizeof(char)*(strlen(ptr_postfix)+20));
		sprintf(outpre, "Prefix: %s\n\0",ptr_prefix);
		sprintf(outpost, "Postfix: %s\n\0",ptr_postfix);
	
		char *result = calculate(ptr_postfix);
		outresult = malloc(sizeof(char)*(strlen(result)+20));
		sprintf(outresult, "Value: %s\n\0",result);
	
		printf("%s\n",outpre);
		printf("%s\n",outpost);
		printf("%s\n",outresult);
	}

/*PROCESSING DONE*/ 


/*OUTPUT*/
  FILE *out;
  out = fopen(outputfilename, "w");
  if(out==NULL){
    printf("File did not open successfully!\n");
	return 1;
  }
  else{
    //printf("File successfully opened.\n");
  }
	if(!errorFound){
		fputs(outpre,out);
		fputs(outpost,out);
		fputs(outresult,out);
		
		free(inputfilename);
		free(outputfilename);
		free(mod);
		
		//free(result);
		
		//free(outpre);
		//free(outpost);
		//free(outresult);
	}
	else{
		fputs("Error!\0",out);
		/*
		free(inputfilename);
		free(outputfilename);
		free(mod);
		*/
	}
  if(fclose(out)==0){
    //printf("File successfully closed.\n");
  }
/*OUTPUT DONE*/  
  return 0;
}

