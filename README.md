# POST project README

Contributors:

 - Matthew Lepore mwlepore@buffalo.edu
 - Xuhui Li-xuhuili@buffalo.edu
 - Christopher Elman ceelman@buffalo.edu

DONE: 
  
  - all filtering functionality
  - prompting a user to save before quitting
  - save only allowing a filename that does not exist
  - overwrite only allowing a filename that already exists
  
NOT DONE:
  
  - including a list of applied filters at beginning of saved output file

INSTRUCTIONS FOR USE: (main executable)

  - run 'make all' to create an executable called 'post'
  - run 'post' with a single argument (the xml file with recall data to be parsed) 

  - [Ex. ./post <filename>]

INSTRUCTIONS FOR TESTING:

  - running 'make runtests' will run the suite of CUnit tests and report results, it will also run gcov on post.c and vector.c and report coverage
  - running 'make runmem' will run the suite of CUnit tests and save the results of running valgrind/memcheck to 'memory.txt'
  - running 'make performance' will run the suite of CUnit tests and save the results of running gprof to 'performance.txt'

NOTES:

  - running 'make tests' may cause a test to fail, specifically the tests involving save/overwrite, as it requires a file to exist/not exist in order to pass